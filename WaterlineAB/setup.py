from setuptools import setup

setup(
    name='WaterlineAB',
    version='0.1',
    packages=['waterline'],
    url='',
    license='',
    author='sharad',
    author_email='sharad@waterlinedata.com',
    description='Integeration between Waterline and Palantir'
)
