import requests
from waterline import ConnectionManager as RestConnection
class TagHelper:
    def getTagAssociations(metastore,tag_id):
        dt = requests.get(metastore.base_url + 'tagassociation/tag/'+tag_id, cookies=RestConnection.wl_cookies,
                          headers=RestConnection.wl_headers)
        for i in dt.json():
            yield i['resourceKey']


    def getTagDomains(metastore):
        dt = requests.get(metastore.base_url + 'tagdomain/all', cookies=RestConnection.wl_cookies,
                          headers=RestConnection.wl_headers)
        for i in dt.json():
            yield i['domain']['name']

    def getTagDomainKey(metastore):
        dt = requests.get(metastore.base_url + 'tagdomain/all', cookies=RestConnection.wl_cookies,
                          headers=RestConnection.wl_headers)
        for i in dt.json():
            yield i['domain']['key']

    def getTagByName(metastore,tag_name):
        for domainName in TagAssociations.getTagDomains(metastore):
            dt = requests.get(metastore.base_url + 'tag/byname',params={'domainName' : domainName, 'tagName' : tag_name}, cookies=RestConnection.wl_cookies,
                              headers=RestConnection.wl_headers)
            if dt.status_code!=404:
                return dt.json()['key']

    def getTagAssociationsByName(metastore,tag_name):
        tag_id = TagAssociations.getTagByName(metastore,tag_name)
        return TagAssociations.getTagAssociationsByTagId(metastore,tag_id)

    def getTagAssociationsByTagId(metastore,tag_id):
        dt = requests.get(metastore.base_url + 'tagassociation/tag/' + tag_id, cookies=RestConnection.wl_cookies,
                          headers=RestConnection.wl_headers)
        for i in dt.json():
            yield i['resourceKey']

    def getResourcesMarkedWithTags(metastore,tagname):
        ta = TagAssociations.getTagAssociationsByName(metastore,tagname)
        for t in ta:
            yield t

    def getResource(metastore,resourcekey):
        dt = requests.get(metastore.base_url + 'dataresource/'+resourcekey,
                          cookies=RestConnection.wl_cookies,
                          headers=RestConnection.wl_headers)
        return dt.json()

    def getTagKeyForDomain(metastore,domainName):
        dt = requests.get(metastore.base_url + 'tagdomain/' + domainName + "/alltags", cookies=RestConnection.wl_cookies,
                          headers=RestConnection.wl_headers)
        j=dt.json()
        if ('tags' in j):
            for p in j['tags']:
                yield p['key']

    def getAllTagAssociations(metastore):
        for dom_key in TagAssociations.getTagDomainKey(metastore):
            for tag_key in TagAssociations.getTagKeyForDomain(metastore,dom_key):
                for res_key in TagAssociations.getTagAssociationsByTagId(metastore,tag_key):
                    yield res_key
