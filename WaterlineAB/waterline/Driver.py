import requests
from waterline import PalantirREST
from optparse import OptionParser
from waterline.ConnectionManager import ConnectionManager


class Driver:
   def get_comma_separated_args(self,option, opt, value, parser):
       setattr(parser.values, option.dest, value.split(','))
   datasets = None
   def start(self):
       self.datasets = {}
       parser = OptionParser()
       parser.add_option('-d', '--datasets',
                         type='string',
                         action='callback',
                         callback=self.get_comma_separated_args,
                         help="List of Palantir Datasets to process",
                         dest=self.datasets)
       #parser.add_option("-q", "--quiet",
       #                  action="store_false", dest="verbose", default=True,
       #                  help="don't print status messages to stdout")

       (options, args) = parser.parse_args()
       print (self.datasets)
       for i in self.datasets:
           print (i)
       r = ConnectionManager()
       r.login()
       pal = PalantirREST(r)
       if self.datasets is None or len(self.datasets) == 0:
          pal.process_all_palantir_datasets()
       else :
          for rid in self.datasets:
              pal.process_dataset(rid)




if __name__ == '__main__':
    d = Driver()
    d.start()


