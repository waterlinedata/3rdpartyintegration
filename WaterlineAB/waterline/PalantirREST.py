from collections import defaultdict

import requests
import json
from waterline import Config as cfg
from waterline.ConnectionManager import ConnectionManager


class PalantirREST:
    palatir_dataset_url='%s/foundry-catalog/api/catalog/datasets/'%(cfg.palantir_base_url)
    palantir_category_id = None

    def __init__(self, connection):
        self.connection = connection
        self.name_to_category = {}
        self.category_to_name = {}
        self.tagname_to_category = defaultdict(list)
        self.tagmap = {}
        self.setup_all_existing_categories()

    def get_schema(self, rid):
        cols = []
        palantir_schema_url='%s/foundry-metadata/api/schemas/datasets/%s/branches/master'%(cfg.palantir_base_url,rid)
        sc = requests.get(palantir_schema_url, headers=self.connection.pl_key, proxies=cfg.palantir_proxy)
        if sc.status_code == 200 and 'schema' in sc.json() :
            for s in sc.json()['schema']['fieldSchemaList']:
                cols.append(s['name'])
        return cols

    def setup_all_existing_categories(self):
        np = None
        palantir_category_url_base = '%s/compass/api/categories' % (cfg.palantir_base_url)
        palantir_category_url=palantir_category_url_base
        print ('Starting loading all categories from Palantir')
        while True:
            if np is not None:
                palantir_category_url = palantir_category_url_base + '?pageToken=%s'%np
            print('Loading URL %s'% palantir_category_url)
            sc = requests.get(palantir_category_url, headers=self.connection.pl_key, proxies=cfg.palantir_proxy)
            if sc.status_code == 200:
                np = sc.json()['nextPageToken']
                for s in sc.json()['values']:
                    cat_id = s['category']['rid']
                    cat_name = s['category']['name']
                    self.name_to_category[cat_name] = cat_id
                    self.category_to_name[cat_id] = cat_name
                    for p in s['tags']:
                        self.tagname_to_category[p['name']] .append([p['category'], p['rid']])
                        self.tagmap[p['rid']] = cat_name, p['name']
            if np is None:
                break
        print ('Finished loading all categories from Palantir. Found %d categories' % len(self.name_to_category))
        for c in self.name_to_category:
            print (c)

    def get_category_name_palantir(self, category_id):
        if category_id in self.category_to_name:
            return self.category_to_name[category_id]

        palantir_category_url = '%s/compass/api/batch/categories/' % (cfg.palantir_base_url)
        headers = self.connection.pl_key
        headers['content-type'] = 'application/json'
        sc = requests.post(palantir_category_url,
                           data=json.dumps([category_id]),
                           headers=headers, proxies=cfg.palantir_proxy)
        if sc.status_code == 200:
            nm = sc.json()[category_id]['category']['name']
            self.category_to_name[category_id] = nm
            self.name_to_category[nm] = category_id
            return nm
        return None

    def get_existing_tag_names_palantir(self, tagidlist):
        tagnames = []
        for trid in tagidlist:
            if trid in self.tagmap:
                r = self.tagmap[trid]
                if r is not None:
                    tagnames.append(r)
        return tagnames

    def getlineage(self,rid):
        parents=[]
        palantir_lineage_url='%s/foundry-build/api/build/datasets/%s/jobSpecs'%(cfg.palantir_base_url,rid)
        lc = requests.get(palantir_lineage_url,headers=self.connection.pl_key, proxies=cfg.palantir_proxy)
        if lc.status_code==200 and 'master' in lc.json() :
            for s in lc.json()['master']['inputs']:
                prid = s['datasetRid']
                pname,tags = self.get_dataset_name(prid)
                print ("Parent Lineage: %s"%(pname))
                if pname is not None:
                    pwid=self.locate_in_waterline(pname,None,False)
                    if pwid is not None:
                        parents.append(pwid)
        return parents

    def get_dataset_name(self,rid):
        tag_uri = '%s/compass/api/resources/%s?decoration=tags' % (cfg.palantir_base_url, rid)
        treq = requests.get(tag_uri, headers=self.connection.pl_key, proxies=cfg.palantir_proxy)
        if treq.status_code != 200:
            print("Can't process:"+rid)
            return None,None
        dsname = treq.json()['name']
        tags = treq.json()['tags']
        return dsname,tags

    def process_dataset(self,rid):
        dsname,tags = self.get_dataset_name(rid)
        if dsname is None:
            return
        schema = self.get_schema(rid)
        wlrsid = self.locate_in_waterline(dsname, schema)
        print(" For child %s getting parent lineages"%(dsname))
        wlparentid = self.getlineage(rid)
        if wlrsid is not None:
            self.push_tags_to_palantir(wlrsid, rid,dsname, tags, schema)
            self.push_lineage_to_waterline(wlrsid,rid,wlparentid)

    def process_all_palantir_datasets(self):
        r = requests.get(self.palatir_dataset_url,headers=self.connection.pl_key, proxies=cfg.palantir_proxy)
        for i in r.json():
            rid = i['rid']
            self.process_dataset(rid)

    def push_tags_to_palantir(self, wlrsid, rid,dsname, existingtagids, schema):
        existingtagnames_palantir = self.get_existing_tag_names_palantir(existingtagids)
        existingtagnames_wl = []
        print("Pushing tags:%s,%s to palantir: Existing Tags(%s)"%(dsname,schema,existingtagnames_palantir))
        r = requests.get(self.connection.base_url + 'tagassociation/resource/%s' % (wlrsid),
                         cookies=self.connection.wl_cookies, headers=self.connection.wl_headers, verify=cfg.wl_cert,
                         proxies=cfg.wl_proxy)
        if r.status_code != 200:
            return
        for ta in r.json():
            r1 = requests.get(self.connection.base_url + 'tag/%s' % ta['tagKey'], cookies=self.connection.wl_cookies,
                              headers=self.connection.wl_headers, verify=cfg.wl_cert,
                              proxies=cfg.wl_proxy)
            if r1.status_code == 200:
                r2 = requests.get(self.connection.base_url + 'tagdomain/%s' % r1.json()['domainKey'], cookies=self.connection.wl_cookies,
                                  headers=self.connection.wl_headers, verify=cfg.wl_cert,
                                  proxies=cfg.wl_proxy)
                if r2.status_code == 200:
                    existingtagnames_wl.append([r2.json()['name'],r1.json()['fullName']])
        self.post_tags_to_palantir(existingtagnames_palantir,existingtagnames_wl,rid)

    def create_or_get_palantir_tag(self, domainname, tagname):
        domainid = None
        print('Looking for Palantir Tag: %s,%s' % (domainname,tagname))
        if domainname not in self.name_to_category:
            print('Creating new category: %s' % domainname)
            palantir_category_url = '%s/compass/api/categories/' % (cfg.palantir_base_url)
            headers = self.connection.pl_key
            headers['content-type'] = 'application/json'
            print('Category url:%s' % palantir_category_url)
            sc = requests.post(palantir_category_url,
                               data=json.dumps({'name': domainname, 'shortDescription': 'Created from Waterline'}),
                               headers=headers, proxies=cfg.palantir_proxy)
            if sc.status_code == 200:
                domainid = sc.json()['rid']
                self.name_to_category[domainname] = domainid
                self.category_to_name[domainid] = domainname
                print("created new category name: %s, id: %s" % (domainname, domainid))
            else:
                print('Unable to create category %s' % sc.status_code)
                return None
        else:
            domainid = self.name_to_category[domainname]

        tag_id = None
        if tagname in self.tagname_to_category:
            for cat_id,t_id in self.tagname_to_category[tagname]:
                if cat_id == domainid:
                     print ('Existing tag found in Palantir id: %s' % t_id)
                     tag_id = t_id
                     break
        if tag_id is None:
            palantir_tag_url = '%s/compass/api/categories/%s/tags' % (cfg.palantir_base_url, domainid)
            headers = self.connection.pl_key
            headers['content-type'] = 'application/json'
            sc = requests.post(palantir_tag_url,
                               data=json.dumps({'name': tagname}),
                               headers=headers, proxies=cfg.palantir_proxy)
            if sc.status_code == 200:
                newtagid = sc.json()['rid']
                print("created new tag name: %s, domain: %s, tag id:%s" %
                      (tagname, domainname, newtagid))
                self.tagname_to_category[tagname].append([domainid, newtagid])
                self.tagmap[newtagid] = domainname, tagname
                return newtagid
        return tag_id

    def create_or_get_palantir_tag_orig(self, tagname, domainname):
        print('Creating new palatir tag')
        if self.palantir_category_id is None:
            print('Creating category')
            palantir_category_url = '%s/compass/api/categories/' % (cfg.palantir_base_url)
            headers = self.connection.pl_key
            headers['content-type'] = 'application/json'
            print('Category url:%s' % palantir_category_url)
            sc = requests.post(palantir_category_url,
                               data=json.dumps({'name': domainname, 'shortDescription': 'Created from Waterline'}),
                               headers=headers, proxies=cfg.palantir_proxy)
            if sc.status_code == 200:
                self.palantir_category_id = sc.json()['rid']
                print("created new category name: %s, id: %s", (domainname, self.palantir_category_id))
            else:
                print('Unable to create category %s' % sc.status_code)
        else:
            palantir_category_url = '%s/compass/api/batch/categories/' % (cfg.palantir_base_url)
            headers = self.connection.pl_key
            headers['content-type'] = 'application/json'
            sc = requests.post(palantir_category_url,
                               data=json.dumps([self.palantir_category_id]),
                               headers=headers, proxies=cfg.palantir_proxy)
            if sc.status_code == 200:
                for tag in sc.json()[self.palantir_category_id]['tags']:
                    if tag['name'] == tagname:
                        return tag['rid']
            else:
                print('Unable to locate tags %s' % sc.status_code)
                return None
        if self.palantir_category_id is not None:
            palantir_tag_url = '%s/compass/api/categories/%s/tags' % (cfg.palantir_base_url, cfg.palantir_category_id)
            headers = self.connection.pl_key
            headers['content-type'] = 'application/json'
            sc = requests.post(palantir_tag_url,
                               date=json.dumps({'name': tagname}),
                               headers=headers, proxies=cfg.palantir_proxy)
            if sc.status_code == 200:
                newtagid = sc.json()['rid']
                print("created new tag name: %s, id: %s, category id:%s",
                      (tagname, domainname, self.palantir_category_id))
                return newtagid
        return None

    def associate_tag_with_resource(self,rid,palantir_tag_id):
        self.change_tag_association(rid,palantir_tag_id,'ADD')

    def remove_tag_from_resource(self,rid,palantir_tag_id):
        self.change_tag_association(rid,palantir_tag_id,'REMOVE')

    def change_tag_association(self,rid,palantir_tag_id,type):
        print("Trying to change tag %s with resource %s type: %s"%(palantir_tag_id,rid,type))
        palantir_tag_assoc_url = '%s/compass/api/tags/resources/%s' % (cfg.palantir_base_url,rid)
        headers = self.connection.pl_key
        headers['content-type'] = 'application/json'
        sc = requests.put(palantir_tag_assoc_url,
                          data=json.dumps({'tagsDiff':{palantir_tag_id:type}}),
                          headers=headers, proxies=cfg.palantir_proxy)
        if sc.status_code == 200:
            print("Successfully associated tag %s with resource %s"%(palantir_tag_id,rid))
        else :
            print ("Tag assocaition faild due to %s"%sc.status_code)

    def post_tags_to_palantir(self, existing_palantir, existing_wl, rid):
        print("Posting tags to palantir:%s"%(existing_wl))
        print("Exising tags in palantir:%s"%(existing_palantir))
        for domain,tag in existing_wl:
            if [domain,tag] not in existing_palantir:
                palantir_tag_id = self.create_or_get_palantir_tag(domain, tag)
                if palantir_tag_id is not None :
                    self.associate_tag_with_resource(rid,palantir_tag_id)
        return

    def push_lineage_to_waterline(self,wlrsid,rid,wlparentid ):
        print("Pushing lineage:%s,%s to waterline"%(wlrsid,wlparentid))
        for anc in wlparentid:
            print("Parent resource id:%s"%anc)
            payload = {'relatedKey': '%s' % anc}
            r = requests.post(self.connection.base_url + 'lineage/addparent/%s'%rid, data=json.dumps(payload),
                              cookies=self.connection.wl_cookies, headers=self.connection.wl_headers,  verify=cfg.wl_cert,
                              proxies=cfg.wl_proxy)
            if r is not None and r.status_code==200:
                print("Lineage created")
            else:
                print("Lineage creation failed")

    def locate_in_waterline(self, dsname, schema,checkschema=True):
        print("Trying to find:%s,%s in waterline" % (dsname, schema))
        payload = {'searchPhrase': '%s' % dsname, 'facetSelections': [], 'pagingCriteria': {'start': 0, 'size': 25},
                   'entityScope': ['data_resource']}
        r = requests.post(self.connection.base_url + 'search/new', data=json.dumps(payload),
                          cookies=self.connection.wl_cookies, headers=self.connection.wl_headers,  verify=cfg.wl_cert,
                          proxies=cfg.wl_proxy)
        if (r.status_code != 200):
            return None
        for dr in r.json()['entities']:
            if dr['resourcePath'].endswith(dsname):
                if checkschema:
                    for f in dr['fields']:
                        if f['name'] not in schema:
                            print("%s Field not found in Palantir for resource %s" % (f['name'], dsname))
                            return None
                print("Located resource %s in waterline with entity id %s" % (dsname, dr['key']))
                return dr['key']
        return None


if __name__ == '__main__':
    r = ConnectionManager()
    r.login()
    pal = PalantirREST(r)
    pal.process_all_palantir_datasets()
