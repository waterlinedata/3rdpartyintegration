import requests
import json
from waterline import Config as cfg
class ConnectionManager:
    wl_cookies = "WDSessionId"
    wl_headers = {'Content-Type': 'application/json; charset=utf8', 'Accept': 'application/json', 'Cache-Control': 'no-cache'}
    def __init__(self, username=cfg.current['user'],password=cfg.current['password'],host=cfg.current['host'],port=cfg.current['port'] ):
        self.username = username
        self.password = password
        self.host = host
        self.port = port
        if port is None:
            self.base_url = 'https://%s/api/v2/'%(self.host)
        else:
            self.base_url = 'https://%s:%s/api/v2/'%(self.host,self.port)
        self.login_url = self.base_url+'login'
        self.wl_proxy = cfg.wl_proxy

        self.pl_key = cfg.palantir_key
        self.pl_proxy = cfg.palantir_proxy
        return


    def login(self):
        payload = {'username':'%s' % self.username, 'password': '%s' % self.password}
        r = requests.post(self.login_url, data=json.dumps(payload), headers=self.wl_headers,verify=False)
        q = r.json()
        print(r.cookies['WDSessionId'])
        if 'loginResult' in q and q['loginResult'] == 'SUCCESS':
            ConnectionManager.wl_cookies = dict(WDSessionId=r.cookies['WDSessionId'])
        else:
            raise Exception('Bad login')



if __name__ == '__main__':
    #r = RestConnection('wlddev','1','localhost','8082')
    r = ConnectionManager()
    r.login()
    print("Cookies are %s"%ConnectionManager.wl_cookies['WDSessionId'])
    ds = requests.get(r.base_url+'datasource', cookies=ConnectionManager.wl_cookies, headers=ConnectionManager.wl_headers,verify=False)

    for i in ds.json():
        print (i)